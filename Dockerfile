# STAGE 1
FROM alpine as builder
RUN apk add --update nodejs npm
RUN mkdir -p /home/node/app/node_modules
WORKDIR /home/node/app
COPY package*.json ./
# RUN npm config set unsafe-perm true
RUN npm install
COPY . .
RUN npm run build

# STAGE 2
FROM alpine
RUN apk add --update nodejs npm curl
RUN mkdir -p /home/node/app/node_modules
WORKDIR /home/node/app
COPY package*.json ./
RUN npm install --only=prod
COPY --from=builder /home/node/app/dist ./dist
EXPOSE $PORT

CMD [ "node", "dist/main.js" ] 