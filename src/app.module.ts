import { Module} from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { TypeOrmModule } from '@nestjs/typeorm';
import { resolve } from 'path';
import { BarberShopModule } from './app/barber-shop/barber-shop.module';
import { BookingModule } from './app/booking/booking.module';
import { PromotionsModule } from './app/promotions/promotions.module';
import { RegionsModule } from './app/regions/regions.module';
import { ReviewsModule } from './app/reviews/reviews.module';
import { ClientModule } from './app/client/client.module';
import { AuthModule } from './app/auth/auth.module';
import { UserModule } from './app/user/user.module';
import { DashboardModule } from './app/dashboard/dashboard.module';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage} from 'multer';
import { typeOrmConfig } from './database/typeorm.config';
import { UtilModule } from './app/util/util.module';
import { ImageModule } from './app/image/image.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(typeOrmConfig),
    ServeStaticModule.forRoot({
      rootPath: resolve('.') + '/uploads'
    }),
    MulterModule.registerAsync({
        useFactory: () => ({
          storage: diskStorage({
            destination: './uploads'
          })
        })
    }),
    BookingModule,
    BarberShopModule,
    PromotionsModule,
    RegionsModule,
    ReviewsModule,
    ClientModule,
    AuthModule,
    UserModule,
    DashboardModule,
    UtilModule,
    ImageModule,
  ]
})
export class AppModule { }
