import { Body, Controller, HttpException, HttpStatus, Ip, Post, Req } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { profileEnd } from 'console';
import { Repository } from 'typeorm';
import { BarberShopService } from '../barber-shop/barber-shop.service';
import { RejectionService } from '../barber-shop/rejection/rejection.service';
import { ProfileStatus } from '../barber-shop/types/salon.type';
import { ClientService } from '../client/client.service';
import { UserType } from '../user/entities/user-type.entity';
import { User } from '../user/entities/user.entity';
import { UserTypes } from '../user/types/user.type';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { GoogleLoginDto } from './dto/google-login.dto';
import { LoginDto } from './dto/login.dto';
import { SendCodeDto } from './dto/send-code.dto';
import { VerifyCodeDto } from './dto/verify-code.dto';
import { Otp } from './entities/otp.entity';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
    private readonly barberService: BarberShopService,
    private readonly clientService: ClientService,
    private readonly rejectionService: RejectionService,
    @InjectRepository(Otp) private readonly otpRepository: Repository<Otp>
  ) { }


  @Post('login')
  async login(@Body() loginDto: LoginDto) {
    const result = await this.authService.sendCode(loginDto.phoneNumber);
    return {
      status: HttpStatus.OK,
      message: result
    }
  }


  @Post('resend-code')
  async sendCode(@Body() { phoneNumber }: SendCodeDto) {
    const result = await this.authService.sendCode(phoneNumber);
    return {
      status: HttpStatus.OK,
      message: result
    }
  }

  @Post('verify-code')
  async verifyCode(@Body() { phoneNumber, code, type }: VerifyCodeDto) {
    let user = await this.authService.verifyCode(phoneNumber, code);



    let rejectionReason;
    let profileStatus;
    if (!user) {
      user = await this.userService.create({
        phoneNumber
      } as User, type);

      switch (type) {
        // TODO: handle client creation
        case UserTypes.CLIENT:
          await this.clientService.create(user);
          break;

        case UserTypes.SHOP:
          await this.barberService.create(user);
          profileStatus = ProfileStatus.PENDING;
          break;
      }
    }

    if(user?.userType?.code === UserTypes.SHOP){
      const barber = await this.barberService.findByUser(user.id);
      profileStatus = barber.profileStatus;
      if(profileStatus === ProfileStatus.REJECTED){
        const rejection = await this.rejectionService.findByBarber(barber.id);
        rejectionReason = rejection.reason;
      }
    }

    return {
      status: HttpStatus.OK,
      message: 'Success',
      data: {
        accessToken: this.authService.login({
          id: user?.id,
          phoneNumber,
          role: user?.userType?.code
        }),
        user: {
          ...user,
        },
        profile: {
          profileStatus,
          rejectionReason
        }
      }
    }
  }

  @Post('/google/login')
  async googleLogin(
    @Body() body: GoogleLoginDto,
    @Req() req,
    @Ip() ip: string,
  ): Promise<{ accessToken: string; refreshToken: string }> {
    const result = await this.authService.loginGoogleUser(body.token, {
      userAgent: req.headers['user-agent'],
      ipAddress: ip,
    });
    console.log(result, 'result');

    if (result) {
      return result;
    } else {
      throw new HttpException(
        {
          status: HttpStatus.UNAUTHORIZED,
          error: 'Error while logging in with google',
        },
        HttpStatus.UNAUTHORIZED,
      );
    }
  }
}
