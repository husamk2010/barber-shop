import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { LocalStrategy } from './strategies/passport-local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ENV } from 'src/config';
import { JwtStrategy } from './strategies/passport-jwt.strategy';
import { PassportModule } from '@nestjs/passport';
import { UserModule } from '../user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Otp } from './entities/otp.entity';
import { BarberShopModule } from '../barber-shop/barber-shop.module';
import { ClientModule } from '../client/client.module';
import { RejectionModule } from '../barber-shop/rejection/rejection.module';

@Module({
  imports: [
    PassportModule,
    JwtModule.register({
      secret: ENV.Auth.JWT_SECRET,
      signOptions: { expiresIn: '60s' }
    }),
    UserModule,
    TypeOrmModule.forFeature([Otp]),
    BarberShopModule,
    ClientModule,
    RejectionModule
  ],
  controllers: [AuthController],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [AuthService]
})
export class AuthModule { }
