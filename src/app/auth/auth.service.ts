import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Auth, google } from 'googleapis';
import { UserService } from '../user/user.service';
import { User } from '../user/entities/user.entity';
import * as moment from 'moment';
import { InjectRepository } from '@nestjs/typeorm';
import { Otp } from './entities/otp.entity';
import { Repository } from 'typeorm';


@Injectable()
export class AuthService {

  private oauthClient: Auth.OAuth2Client; // add this

  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UserService,
    @InjectRepository(Otp) private readonly otpRepository: Repository<Otp>
  ) {
    const clientId = process.env.GOOGLE_CLIENT_ID;
    const clientSecret = process.env.GOOGLE_CLIENT_SECRET;
    this.oauthClient = new google.auth.OAuth2(clientId, clientSecret);
  }

  async loginGoogleUser(
    token: string,
    values: { userAgent: string; ipAddress: string },
  ): Promise<any | { accessToken: string; refreshToken: string } | undefined> {
    const ticket = await this.oauthClient.verifyIdToken({
      idToken: token,
      audience: process.env.GOOGLE_CLIENT_ID
    });

    const { email, name, picture } = ticket.getPayload();
    // const user = await this.usersService.createOrUpdate({
    //   email,
    //   signedUpWithGoogle: true
    // } as User);

    // return user;
  }

  login(jwtPayload: any) {
    console.log(jwtPayload, 'jwtPayload');

    return this.jwtService.sign(jwtPayload);
  }


  async sendCode(phoneNumber: string) {

    // TODO: check this logic later
    // const user = await this.usersService.findOneBy({ phoneNumber });
    // if (!user) {
    //   throw new NotFoundException('user not found')
    // }

    // TODO: handle send sms code

    await this.otpRepository.save({
      phoneNumber: phoneNumber,
      code: Math.ceil(Math.random() * 999999),
      createdAt: moment().toDate()
    });

    return "Code sent successfully"
  }




  async verifyCode(phoneNumber: string, code: number) {
    const userOtp = await this.otpRepository.findOne({
      where: {
        phoneNumber
      },
      order: {
        id: 'DESC'
      }
    });


    if (!userOtp) {
      throw new NotFoundException('user not found')
    }

    if (moment(userOtp.createdAt).add(1, 'minute').unix() <= moment().unix()) {
      throw new BadRequestException('expired code');
    }

    if (userOtp.attempts === 5) {
      throw new BadRequestException('exceeded try limit')
    }

    // TODO: remove later
    if(code === 55555){
      return this.usersService.findOneBy({ phoneNumber })
    }

    if (userOtp.code !== code) {
      await this.otpRepository.update(userOtp.id, {
        attempts: userOtp.attempts + 1
      });

      throw new BadRequestException('invalid code')
    }

    return this.usersService.findOneBy({ phoneNumber })
  }
}