import { IsNotEmpty, IsPhoneNumber } from "class-validator";

export class LoginDto {

    @IsPhoneNumber(null, { message: 'Invalid phone number'})
    @IsNotEmpty()
    phoneNumber: string;
}