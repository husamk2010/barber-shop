import { IsNotEmpty, IsPhoneNumber } from "class-validator";

export class SendCodeDto {
    
    @IsPhoneNumber(null, { message: 'Invalid phone number'})
    @IsNotEmpty()
    phoneNumber: string;
}