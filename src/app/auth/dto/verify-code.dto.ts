import { BadRequestException } from "@nestjs/common";
import { IsNotEmpty, IsNumber, IsPhoneNumber, ValidateIf } from "class-validator";
import { UserTypes } from "src/app/user/types/user.type";

export class VerifyCodeDto {

    @IsPhoneNumber(null, { message: 'Invalid phone number'})
    @IsNotEmpty()
    phoneNumber: string;

    @IsNumber()
    @IsNotEmpty()
    code: number;


    @ValidateIf((obj, val) => {
        if (val === UserTypes.SHOP || val === UserTypes.CLIENT) {
            return true;
        }

        throw new BadRequestException('Invalid type, please choose valid value')
    })
    @IsNotEmpty()
    type: UserTypes;
}