import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Otp {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    code: number;

    @Column()
    createdAt: Date;

    @Column({ default: 0})
    attempts: number;
    
    @Column()
    phoneNumber: string;
}