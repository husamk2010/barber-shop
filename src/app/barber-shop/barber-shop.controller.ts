import { Body, Controller, Get, Post, Req, UseGuards, UseInterceptors, Delete, Param, NotFoundException, Query, DefaultValuePipe, ParseIntPipe } from '@nestjs/common';
import { BarberShopService } from './barber-shop.service';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';
import { UploadInterceptor } from 'src/core/interceptors/upload.interceptor';
import { JwtAuthGuard } from 'src/core/guards/jwt-auth.guard';
import { UpdateBarberDto } from './dtos/update-barber-shop.dto';
import { UserTypes } from '../user/types/user.type';
import { Profile } from 'src/core/decorators/profile.decorator';
import { ProfileGuard } from 'src/core/guards/profile.guard';
import { Roles } from 'src/core/decorators/roles.decorator';
import { RolesGuard } from 'src/core/guards/roles.guard';
import { BarberDecorator } from 'src/core/decorators/barber.decorator';
import { BarberShop } from './entities/barber.entity';
import { ProfileStatus } from './types/salon.type';
import { MediaSize, MediaType, Media_Size, Media_Type } from 'src/core/decorators/media-type.decorator';
import { BarberSearchDto } from './dtos/barber-search.dto';


@Controller('barber')
export class BarberShopController {
  constructor(
    private readonly barberShopService: BarberShopService
  ) { }


  @UseGuards(JwtAuthGuard)
  @Get('')
  async search(
    @Query('page', new DefaultValuePipe(1), ParseIntPipe) page: number = 1,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit: number = 10,
    @Query() { name, bio, lat, long, distance }: BarberSearchDto,
  ) {
    const result = await this.barberShopService.search({
      page,
      limit
    }, {
      name,
      bio,
      lat,
      long,
      distance
    });
    return {
      message: 'Success',
      data: result
    }
  }



  @Roles(UserTypes.SHOP)
  @Profile(ProfileStatus.PENDING, ProfileStatus.AWAITING_REVIEW)
  @UseGuards(JwtAuthGuard, RolesGuard, ProfileGuard)
  @MediaType(Media_Type.FILES)
  @MediaSize(Media_Size._10MEGABYTE)
  @UseInterceptors(FilesInterceptor('files'), UploadInterceptor)
  @Post('upload/files')
  uploadFiles(@BarberDecorator() barber: BarberShop, @Req() req) {

    if (barber.profileStatus === ProfileStatus.PENDING) {
      return this.barberShopService.uploadFiles(barber, req.uploadedFiles);
    }

    return {

      message: 'Awaiting review',
      profileStatus: barber.profileStatus
    }
  }

  // Barber Profile
  @Roles(UserTypes.SHOP)
  @Profile(ProfileStatus.APPROVED)
  @UseGuards(JwtAuthGuard, RolesGuard, ProfileGuard)
  @Post('profile')
  async updateProfile(@BarberDecorator() barber: BarberShop, @Body() updateBarberDto: UpdateBarberDto) {
    await this.barberShopService.updateProfile(barber, updateBarberDto);
    return {
      message: 'Success'
    }
  }

  @Roles(UserTypes.SHOP)
  @Profile(ProfileStatus.APPROVED)
  @UseGuards(JwtAuthGuard, RolesGuard, ProfileGuard)
  @MediaType(Media_Type.MEDIA)
  @MediaSize(Media_Size._10MEGABYTE)
  @UseInterceptors(FileInterceptor('cover'), UploadInterceptor)
  @Post('profile/cover')
  async updateProfileCover(@BarberDecorator() barber: BarberShop, @Req() req) {
    await this.barberShopService.updateCover(barber, req.uploadedFiles[0]);
    return {
      message: 'Success'
    }
  }


  @Roles(UserTypes.SHOP)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id/profile')
  async getProfile(@Param('id') id) {
    const profile = await this.barberShopService.getProfile(id);
    return {
      message: 'Success',
      data: profile
    }
  }


  @Roles(UserTypes.SHOP)
  @Profile(ProfileStatus.APPROVED)
  @MediaType(Media_Type.MEDIA)
  @MediaSize(Media_Size._10MEGABYTE)
  @UseGuards(JwtAuthGuard, RolesGuard, ProfileGuard)
  @UseInterceptors(FilesInterceptor('media', 5), UploadInterceptor)
  @Post('gallery')
  async uploadMedia(@BarberDecorator() barber: BarberShop, @Req() req) {
    await this.barberShopService.uploadMedia(barber, req.uploadedFiles);
    return {
      message: 'Success'
    }
  }

  @Roles(UserTypes.CLIENT, UserTypes.SHOP, UserTypes.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id/gallery')
  async getGallery(@Param('id') id) {
    const barberObject = await this.barberShopService.getGallery(id);
    return {
      message: 'Success',
      data: {
        gallery: barberObject.gallery
      }
    }
  }


  @Roles(UserTypes.CLIENT, UserTypes.SHOP, UserTypes.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id/services')
  async getServices(@Param('id') id) {
    const services = await this.barberShopService.getBarberServices(id);
    return {
      message: 'Success',
      data: {
        services
      }
    }
  }

  @Roles(UserTypes.SHOP)
  @Profile(ProfileStatus.APPROVED)
  @UseGuards(JwtAuthGuard, RolesGuard, ProfileGuard)
  @Delete('gallery/:id')
  async deleteImageFromGallery(@BarberDecorator() barber: BarberShop, @Param('id') id) {
    await this.barberShopService.deleteImageFromGallery(barber, +id);
    return {
      message: 'Success'
    }
  }


  @UseGuards(JwtAuthGuard, RolesGuard)
  @Get(':id')
  async getById(@Param('id') id) {
    const barber = await this.barberShopService.findOne(+id);
    if (!barber) {
      throw new NotFoundException('barber not found')
    }

    return {
      message: 'Success',
      data: barber
    }
  }
}
