import { Module } from '@nestjs/common';
import { BarberShopService } from './barber-shop.service';
import { BarberShopController } from './barber-shop.controller';
import { AuthModule } from '../auth/auth.module';
import { UserModule } from '../user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BarberShop } from './entities/barber.entity';
import { Location } from './entities/location.entity';
import { OpenHours } from './entities/open-hours.entity';
import { Document } from './entities/document.entity';
import { BarberService } from './entities/barber-service.entity';
import { SalonServiceService } from './salon-service/salon-service.service';
import { SalonServiceModule } from './salon-service/salon-service.module';
import { Service } from './entities/service.entity';
import { ImageModule } from '../image/image.module';
import { RejectionModule } from './rejection/rejection.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([BarberShop, Location, OpenHours, Document, BarberService, Service]),
    UserModule, 
    SalonServiceModule,
    ImageModule,
    RejectionModule
  ],
  controllers: [BarberShopController],
  providers: [BarberShopService, SalonServiceService],
  exports: [BarberShopService, SalonServiceService]
})
export class BarberShopModule {}
