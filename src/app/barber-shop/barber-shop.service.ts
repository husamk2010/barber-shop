import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateBarberDto } from './dtos/update-barber-shop.dto';
import { BarberShop } from './entities/barber.entity';
import { OpenHours } from './entities/open-hours.entity';
import { Location } from './entities/location.entity';
import { Document } from './entities/document.entity';
import { BarberService } from './entities/barber-service.entity';
import { ImageService } from '../image/image.service';
import { Image } from '../image/entities/image.entity';
import { ProfileStatus } from './types/salon.type';
import { IPaginationOptions, paginate } from 'nestjs-typeorm-paginate';
import { BarberSearchDto } from './dtos/barber-search.dto';
import { SEARCH_DISTANCE } from '../constants';


@Injectable()
export class BarberShopService {
    constructor(
        @InjectRepository(BarberShop) private readonly barberRepository: Repository<BarberShop>,
        @InjectRepository(Location) private readonly locationRepository: Repository<Location>,
        @InjectRepository(OpenHours) private readonly openHoursRepository: Repository<OpenHours>,
        @InjectRepository(Document) private readonly documentsRepository: Repository<Document>,
        @InjectRepository(BarberService) private readonly barberServicesRepository: Repository<BarberService>,
        private readonly imageService: ImageService
    ) { }


    async search(options: IPaginationOptions, { lat, long, name, bio, distance }: BarberSearchDto) {

        // if lat and long provided 
        // then retrieve all barbers located with specific distance 
        // default is 200km
        if (lat && long) {
            const result = await this.barberRepository
                .query(`call GetLocationsWithinDistance(${lat}, ${long}, ${distance ?? SEARCH_DISTANCE})`);
            return result[0];
        }


        // otherwise search by (name, bio)
        const queryBuilder = this.barberRepository.createQueryBuilder('barber');
        queryBuilder
            .select(['barber.id', 'barber.name', 'barber.bio'])

        if (name) {
            queryBuilder.orWhere(`barber.name like '%${name}%'`)
        }
        if (bio) {
            queryBuilder.orWhere(`barber.bio like '%${bio}%'`)
        }

        queryBuilder.orderBy('barber.id', 'DESC');

        return paginate(queryBuilder, options)
    }


    find() {
        return this.barberRepository.find({
            relations: {
                user: true,
                services: true,
                openHours: true,
                location: true,

            }
        })
    }

    findByUser(id: number) {
        return this.barberRepository.findOne({
            where: {
                user: { id }
            },
            relations: {
                user: true,
                cover: true
            }
        })
    }

    findOne(id: number) {
        return this.barberRepository.findOne({
            where: { id }
        });
    }


    update(id: number, barber: Partial<BarberShop>) {
        return this.barberRepository.update(id, barber);
    }

    async create(user: any) {
        const barber = await this.barberRepository.findOneBy({ user });
        if (barber) {
            throw new BadRequestException('barber already exists')
        }

        return this.barberRepository.save({
            user
        });
    }

    async updateProfile(barberPayload: BarberShop, { name, openHours, location, type, services }: UpdateBarberDto) {
        const barber = await this.barberRepository.findOne({
            where: {
                id: barberPayload.id
            },
            relations: {
                location: true,
                openHours: true
            }
        });

        let barberOpenHours;
        if (openHours?.length) {
            // remove old records
            await this.openHoursRepository.delete({ barber });

            // create new records
            barberOpenHours = openHours.map(item => ({ ...item, barber }))
            await this.openHoursRepository.save(barberOpenHours);
        }

        let barberLocations;
        if (location?.length) {
            await this.locationRepository.delete({ barber })

            barberLocations = location.map(item => ({ ...item, barber }))
            await this.locationRepository.save(barberLocations);
        }

        let barberServices;
        if (services?.length) {
            await this.barberServicesRepository.delete({ barber });

            barberServices = services.map(service => ({ ...service, barber }));
            await this.barberServicesRepository.save(barberServices);
        }

        await this.barberRepository.save({
            ...barber,
            name,
            type,
            openHours: barberOpenHours,
            location: barberLocations
        });

        // if (user.profileStatus !== ProfileStatus.AWAITING_REVIEW) {
        //     await this.userService.update(user.id, {
        //         profileStatus: ProfileStatus.AWAITING_REVIEW
        //     })
        // }
    }


    async uploadFiles(barber: BarberShop, files: any[]) {
        await this.documentsRepository.save(files.map(file => ({
            name: file.name,
            url: file.url,
            barber
        })));

        if (barber.profileStatus !== ProfileStatus.APPROVED)
            await this.barberRepository.update(barber.id, { profileStatus: ProfileStatus.AWAITING_REVIEW });

        return {
            message: 'Uploaded successfully'
        }
    }


    async getProfile(barberId: number) {
        const profile = await this.barberRepository.findOne({
            where: { id: barberId },
            select: {
                user: {
                    id: true,
                    phoneNumber: true,
                },
                cover: {
                    url: true
                }
            },
            relations: {
                cover: true,
                user: true,
                location: true,
                openHours: true,
            }
        });

        if (!profile) {
            throw new NotFoundException('barber not found')
        }

        return profile;
    }


    async updateCover(barber: BarberShop, cover: Image) {
        if (barber.cover) {
            const coverId = barber.cover.id;
            barber.cover = null;
            await this.barberRepository.save(barber);
            await this.imageService.delete(coverId);
        }

        barber.cover = await this.imageService.create(cover);;
        await this.barberRepository.save(barber);
    }

    async getBarberServices(barberId: number) {
        const barber = await this.barberRepository.findOne({
            where: {
                id: barberId
            },
            select: {
                services: {
                    id: true,
                    cost: true,
                    service: {
                        title: true
                    },
                }
            },
            relations: {
                user: true,
                services: {
                    service: true
                },
            }
        });

        if (!barber) {
            throw new NotFoundException('barber not found')
        }

        return barber.services;
    }


    async uploadMedia(barber: BarberShop, files: any[]) {
        const media = await this.imageService.createList(files);
        barber.gallery = [...media];
        await this.barberRepository.save(barber);

        return {
            message: 'Uploaded successfully'
        }
    }

    async getGallery(barberId: number) {
        const barberObject = await this.barberRepository.find({
            where: {
                id: barberId
            },
            select: {
                gallery: {
                    id: true,
                    url: true
                }
            },
            relations: {
                gallery: true
            }
        });

        if (!barberObject) {
            throw new NotFoundException('barber not found');
        }

        return barberObject[0];
    };

    async deleteImageFromGallery(barber: BarberShop, imageId: number) {
        const barberObject = await this.getGallery(barber.id);

        barberObject.gallery.filter(image => image.id !== imageId);

        await this.barberRepository.save(barberObject);
        await this.imageService.delete(imageId);
    }
}
