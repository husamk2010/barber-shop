import { Type } from "class-transformer";
import {  IsNumber, IsOptional, IsString } from "class-validator";

export class BarberSearchDto {

    @IsOptional()
    @IsString()
    name?: string;

    @IsOptional()
    @IsString()
    bio?: string;

    @IsNumber()
    @IsOptional()
    @Type(() => Number)
    lat?: number;

    @IsNumber()
    @IsOptional()
    @Type(() => Number)
    long?: number;

    @IsNumber()
    @IsOptional()
    @Type(() => Number)
    distance?: number;
}