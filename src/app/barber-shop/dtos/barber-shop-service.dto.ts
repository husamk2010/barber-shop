import { IsNotEmpty, IsNumber, IsString } from "class-validator";

export class BarberShopServiceDto {

    @IsNotEmpty()
    @IsString()
    title: string;

    @IsNotEmpty()
    @IsNumber()
    cost: number;

    @IsNotEmpty()
    @IsNumber()
    durationOfService: number;
}