import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateLocationDto {

    @IsNotEmpty()
    @IsNumber()
    lat: number;


    @IsNotEmpty()
    @IsNumber()
    long: number
}