import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateServiceDto {

    @IsNotEmpty()
    @IsNumber()
    service: number;

    @IsNotEmpty()
    @IsNumber()
    cost: number;
}