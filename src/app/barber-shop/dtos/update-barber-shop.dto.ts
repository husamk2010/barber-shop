import { Type } from "class-transformer";
import { IsString, IsOptional, ValidateNested, IsNotEmpty, IsEnum } from "class-validator";
import { OpenHours } from "../entities/open-hours.entity";
import { SalonType } from "../types/salon.type";
import { CreateLocationDto } from "./create-location.dto";
import { CreateServiceDto } from "./create-service.dto";

export enum WeekDays {
    SATURDAY = 'saturday',
    SUNDAY = 'sunday',
    MONDAY = 'monday',
    TUESDAY = 'tuesday',
    WEDNESDAY = 'wednesday',
    THURSDAY = 'thursday',
    FRIDAY = 'friday'
}

export class UpdateBarberDto{

    @IsNotEmpty()
    @IsString()
    @IsOptional()
    name?: string;

    @IsOptional()
    @ValidateNested({ each: true})
    @Type(() => CreateLocationDto)
    location?: [CreateLocationDto];

    @IsOptional()
    @IsNotEmpty()
    @IsEnum(SalonType)
    type?: SalonType;

    @IsOptional()
    @ValidateNested({ each: true})
    @Type(() => OpenHours)
    openHours?: [OpenHours];


    @IsOptional()
    @ValidateNested({ each: true})
    @Type(() => CreateServiceDto)
    services: [CreateServiceDto]
}