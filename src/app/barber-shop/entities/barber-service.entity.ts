import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { BarberShop } from "./barber.entity";
import { Service } from "./service.entity";

@Entity()
export class BarberService {

    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Service, (service) => service.id)
    service: Service;

    @ManyToOne(() => BarberShop, (barber) => barber.id)
    barber: BarberShop;

    @Column()
    cost: number;
}