import { Image } from "src/app/image/entities/image.entity";
import { User } from "src/app/user/entities/user.entity";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { ProfileStatus, SalonType } from "../types/salon.type";
import { BarberService } from "./barber-service.entity";
import { Location } from "./location.entity";
import { OpenHours } from "./open-hours.entity";
import { Document } from "./document.entity";
import { Appointment } from "src/app/booking/entities/appointment.entity";


@Entity()
export class BarberShop {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true})
    name: string;

    @Column({ default: ''})
    bio: string;

    @ManyToOne(() => Image, (image) => image.id)
    cover: Image;

    @Column({
        type: 'enum',
        enum: SalonType
    })
    type: SalonType;

    @Column({
        type: 'enum',
        enum: ProfileStatus,
        default: ProfileStatus.PENDING
    })
    profileStatus: ProfileStatus;

    // foreign keys
    
    @OneToMany(() => Location, (location) => location.barber)
    location: Location[];

    @OneToMany(() => OpenHours, (openHours) => openHours.barber)
    openHours: OpenHours[];

    @OneToMany(() => BarberService, (barberService) => barberService.barber)
    services: BarberService[]

    @OneToOne(() => User, (user) => user.id)
    @JoinColumn()
    user: User;

    @OneToMany(() => Document, (document) => document.barber)
    documents: Document[];

    @OneToMany(() => Appointment, (appointment) => appointment.barber)
    appointments: Appointment[];

    @ManyToMany(() => Image)
    @JoinTable()
    gallery: Image[]

}