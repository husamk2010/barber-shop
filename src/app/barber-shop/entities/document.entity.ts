import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { BarberShop } from "./barber.entity";

@Entity()
export class Document {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    url: string;

    @ManyToOne(() => BarberShop, (barber) => barber.id)
    barber: BarberShop;
}