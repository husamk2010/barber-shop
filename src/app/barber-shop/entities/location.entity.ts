import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { BarberShop } from "./barber.entity";


@Entity()
export class Location {
    @PrimaryGeneratedColumn()
    id: number; 

    @Column({
        type: 'double'
    })
    lat: number;

    @Column({
        type: 'double'
    })
    long: number

    @Column({ default: ''})
    text: string;

    @ManyToOne(() => BarberShop, (barber) => barber.id)
    barber: BarberShop;
}