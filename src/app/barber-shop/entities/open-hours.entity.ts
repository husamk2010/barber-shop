import { IsEnum, IsNotEmpty, IsNumber } from "class-validator";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { WeekDays } from "../dtos/update-barber-shop.dto";
import { BarberShop } from "./barber.entity";


@Entity()
export class OpenHours {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsNumber()
    @IsNotEmpty()
    from: string;

    @Column()
    @IsNumber()
    @IsNotEmpty()
    to: string;

    @Column()
    @IsEnum(WeekDays)
    day: WeekDays;

    @ManyToOne(() => BarberShop, (barber) => barber.id)
    barber: BarberShop;
}