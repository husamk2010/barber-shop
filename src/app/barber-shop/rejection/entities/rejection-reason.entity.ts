import { Image } from "src/app/image/entities/image.entity";
import { User } from "src/app/user/entities/user.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { BarberShop } from "../../entities/barber.entity";


@Entity()
export class RejectionReason {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    reason: string;

    @ManyToOne(() => BarberShop, (barber) => barber.id)
    barber: BarberShop;
}