import { Module } from '@nestjs/common';
import { RejectionService } from './rejection.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RejectionReason } from './entities/rejection-reason.entity';

@Module({
  imports: [TypeOrmModule.forFeature([RejectionReason])],
  providers: [RejectionService],
  exports: [RejectionService]
})
export class RejectionModule {}
