import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BarberShop } from '../entities/barber.entity';
import { RejectionReason } from './entities/rejection-reason.entity';

@Injectable()
export class RejectionService {
    constructor(
        @InjectRepository(RejectionReason) private readonly rejectionsRepository: Repository<RejectionReason>
    ) { }

    create(reason: string, barber: BarberShop){
        return this.rejectionsRepository.save({ reason, barber});
    }

    find(){
        return this.rejectionsRepository.find();
    }

    findByBarber(barberId:number){
        return this.rejectionsRepository.findOne({
            order: {id: 'DESC'},
            where: { barber: { id: barberId}}
        })
    }

}
