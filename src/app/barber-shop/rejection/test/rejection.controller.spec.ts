import { Test, TestingModule } from '@nestjs/testing';
import { RejectionController } from '../rejection.controller';
import { RejectionService } from '../rejection.service';

describe('RejectionController', () => {
  let controller: RejectionController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [RejectionController],
      providers: [RejectionService],
    }).compile();

    controller = module.get<RejectionController>(RejectionController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
