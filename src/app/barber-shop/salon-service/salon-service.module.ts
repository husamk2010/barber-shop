import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Service } from '../entities/service.entity';
import { SalonServiceService } from './salon-service.service';

@Module({
    imports: [TypeOrmModule.forFeature([Service])],
    providers: [SalonServiceService],
    exports: [SalonServiceService]
})
export class SalonServiceModule {}
