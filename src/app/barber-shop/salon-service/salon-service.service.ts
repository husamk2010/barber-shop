import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { In, Repository } from 'typeorm';
import { Service } from '../entities/service.entity';

@Injectable()
export class SalonServiceService {
    constructor(@InjectRepository(Service) private readonly barberServiceRepository: Repository<Service>){}

    getServices(ids?: number[]){
        return this.barberServiceRepository.find({ where: {
            id: In(ids)
        }});
    }
}
