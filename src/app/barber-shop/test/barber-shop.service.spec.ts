import { Test, TestingModule } from '@nestjs/testing';
import { BarbarShopService } from './barber-shop.service';

describe('BarbarShopService', () => {
  let service: BarbarShopService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BarbarShopService],
    }).compile();

    service = module.get<BarbarShopService>(BarbarShopService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
