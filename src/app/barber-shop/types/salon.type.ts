export enum SalonType {
    MEN,
    WOMEN,
    BOTH
}

export enum ProfileStatus {
    PENDING,
    AWAITING_REVIEW,
    APPROVED,
    REJECTED
}