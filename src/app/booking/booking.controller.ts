import { Body, Controller, Get, HttpStatus, Param, ParseIntPipe, Patch, Post, Put, Query, UseGuards } from '@nestjs/common';
import { BarberDecorator } from 'src/core/decorators/barber.decorator';
import { ClientDecorator } from 'src/core/decorators/client.decorator';
import { Roles } from 'src/core/decorators/roles.decorator';
import { BarberGuard } from 'src/core/guards/barber.guard';
import { ClientGuard } from 'src/core/guards/client.guard';
import { JwtAuthGuard } from 'src/core/guards/jwt-auth.guard';
import { RolesGuard } from 'src/core/guards/roles.guard';
import { UserTypes } from '../user/types/user.type';
import { BookingService } from './booking.service';
import { AcceptCancelAppointmentDto } from './dtos/accept-cancel-appointment.dto';
import { CreateAppointmentDto } from './dtos/create-appointment.dto';
import { UpdateAppointmentDto } from './dtos/update-appointment.dto';

@Controller('booking')
export class BookingController {
  constructor(private readonly bookingService: BookingService) { }


  @Roles(UserTypes.CLIENT)
  @UseGuards(JwtAuthGuard, RolesGuard, ClientGuard)
  @Post('create')
  async create(@ClientDecorator() client, @Body() createAppointmentDto: CreateAppointmentDto) {
    const result = await this.bookingService.create(client, createAppointmentDto);
    return {
      status: HttpStatus.OK,
      message: result
    }
  }


  @Roles(UserTypes.CLIENT)
  @UseGuards(JwtAuthGuard, RolesGuard, ClientGuard)
  @Patch(':id')
  async update(
    @ClientDecorator() client,
    @Body() updateAppointmentDto: UpdateAppointmentDto,
    @Param('id') id
  ) {

    const result = await this.bookingService.update(client, updateAppointmentDto, +id);
    return {
      status: HttpStatus.OK,
      message: result
    }
  }


  @Roles(UserTypes.CLIENT)
  @UseGuards(JwtAuthGuard, RolesGuard, ClientGuard)
  @Post('cancel')
  async cancel(@ClientDecorator() client, @Body() { id }: AcceptCancelAppointmentDto) {
    const result = await this.bookingService.cancel(client, id);
    return {
      status: HttpStatus.OK,
      message: result
    }
  }


  @Roles(UserTypes.SHOP)
  @UseGuards(JwtAuthGuard, RolesGuard, BarberGuard)
  @Post('accept')
  async accept(@BarberDecorator() barber, @Body() { id }: AcceptCancelAppointmentDto) {
    const result = await this.bookingService.accept(barber, id);
    return {
      status: HttpStatus.OK,
      message: result
    }
  }


  @Roles(UserTypes.SHOP)
  @UseGuards(JwtAuthGuard, RolesGuard, BarberGuard)
  @Post('reject')
  async reject(@BarberDecorator() barber, @Body() { id }: AcceptCancelAppointmentDto) {
    const result = await this.bookingService.reject(barber, id);
    return {
      status: HttpStatus.OK,
      message: result
    }
  }


  @Get('barber/appointments')
  @UseGuards(JwtAuthGuard)
  async getBarberAppointments(
    @Query('barberId', new ParseIntPipe()) barberId: number,
  ) {
    const appointments = await this.bookingService.getBarberAppointments(barberId);
    return {
      status: HttpStatus.OK,
      message: 'Success',
      data: appointments
    }
  }



  @Get('client/appointments')
  @Roles(UserTypes.CLIENT)
  @UseGuards(JwtAuthGuard, RolesGuard, ClientGuard)
  async getClientAppointments(
    @ClientDecorator() client
  ) {
    const appointments = await this.bookingService.getClientAppointments(client);
    return {
      status: HttpStatus.OK,
      message: 'Success',
      data: appointments
    }
  }



}
