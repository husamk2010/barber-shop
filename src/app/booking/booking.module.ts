import { Module } from '@nestjs/common';
import { BookingService } from './booking.service';
import { BookingController } from './booking.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Appointment } from './entities/appointment.entity';
import { BarberShopModule } from '../barber-shop/barber-shop.module';
import { UserModule } from '../user/user.module';
import { ClientModule } from '../client/client.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Appointment]),
    BarberShopModule,
    UserModule,
    ClientModule
  ],
  controllers: [BookingController],
  providers: [BookingService]
})
export class BookingModule {}
