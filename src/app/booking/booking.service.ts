import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as moment from 'moment';
import { Repository, Between } from 'typeorm';
import { BarberShopService } from '../barber-shop/barber-shop.service';
import { BarberShop } from '../barber-shop/entities/barber.entity';
import { Client } from '../client/entities/client.entity';
import { CreateAppointmentDto } from './dtos/create-appointment.dto';
import { UpdateAppointmentDto } from './dtos/update-appointment.dto';
import { Appointment, AppointmentStatus } from './entities/appointment.entity';

@Injectable()
export class BookingService {
    constructor(
        @InjectRepository(Appointment) private readonly appointmentRepository: Repository<Appointment>,
        private readonly barberService: BarberShopService
    ) { }

    async getBarberAppointments(barberId: number) {
        const bookings = await this.appointmentRepository.find({
            where: {
                barber: { id: barberId }
            },
            relations: {
                services: {
                    service: true
                },
                client: true
            }
        })

        return bookings;
    }


    async getClientAppointments(client: Client) {
        const bookings = await this.appointmentRepository.find({
            where: {
                client: { id: client.id }
            },
            relations: {
                services: {
                    service: true
                },
                barber: true
            }
        })

        return bookings;
    }

    async create(client: Client, { barberId, bookedAt, services }: CreateAppointmentDto) {
        const barber = await this.barberService.findOne(barberId);
        if (!barber) {
            throw new NotFoundException('barber not found');
        }

        const num = services.map(item => item.id) as number[];
        const barberServices = (await this.barberService.getBarberServices(barberId)).filter(service => num.find(item => item === service.id));

        if (!barberServices.length) {
            throw new BadRequestException('services is not available')
        }

        const todayDate = moment();
        const todayStartDate = todayDate.startOf('day').toDate();
        const todayEndDate = todayDate.endOf('day').toDate()

        const hasBookingToday = await this.appointmentRepository.count({
            where: {
                client: { id: client.id },
                barber: { id: barber.id },
                bookedAt: Between(todayStartDate, todayEndDate)
            }
        });

        if (hasBookingToday) {
            throw new BadRequestException('you already booked at this barber')
        }

        const appointment = await this.appointmentRepository.save({
            bookedAt: new Date(bookedAt),
            barber,
            client
        });

        appointment.services = barberServices;

        await this.appointmentRepository.save(appointment);

        return "Appointment created successfully"
    }


    async update(client: Client, { barberId, bookedAt, services }: UpdateAppointmentDto, id: number) {
        const appointment = await this.appointmentRepository.findOneBy({ id });
        if (!appointment) {
            throw new NotFoundException('appointment not found')
        }

        const barber = await this.barberService.findOne(barberId);
        if (!barber) {
            throw new NotFoundException('barber not found');
        }

        const num = services.map(item => item.id) as number[];
        const barberServices = (await this.barberService.getBarberServices(barberId)).filter(service => num.find(item => item === service.id));

        if (!barberServices.length) {
            throw new BadRequestException('services is not available')
        }

        await this.appointmentRepository.save({
            id,
            bookedAt: new Date(bookedAt),
            barber,
            client,
            status: AppointmentStatus.PENDING,
            services: barberServices
        });

        return "Appointment updated successfully"
    }

    async accept(barber: BarberShop, appointmentId: number) {
        // TODO: notify the client
        const result = await this.appointmentRepository.update({
            id: appointmentId,
            barber: { id: barber.id }
        }, {
            status: AppointmentStatus.ACCEPTED
        });

        return result.affected ? 'Accepted successfully' : 'No appointments match'
    }

    async reject(barber: BarberShop, appointmentId: number) {
        // TODO: notify the client
        const result = await this.appointmentRepository.update({
            id: appointmentId,
            barber: { id: barber.id }
        }, {
            status: AppointmentStatus.CANCELLED
        });

        return result.affected ? 'Rejected successfully' : 'No appointments match'
    }

    async cancel(client: Client, appointmentId: number) {
        const result = await this.appointmentRepository.update({
            id: appointmentId,
            client: { id: client.id }
        }, { status: AppointmentStatus.CANCELLED });

        return result.affected ? 'Cancelled successfully' : 'No appointments match'
    }
}
