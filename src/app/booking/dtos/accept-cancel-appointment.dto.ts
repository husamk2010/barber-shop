import { IsNotEmpty, IsNumber } from "class-validator";


export class AcceptCancelAppointmentDto {

    @IsNotEmpty()
    @IsNumber()
    id: number;
}