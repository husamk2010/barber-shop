import { Transform, Type } from "class-transformer";
import { IsDate, isDateString, IsDateString, IsNotEmpty, IsNumber, MinDate, ValidateNested } from "class-validator";
import { CreateServiceDto } from "./create-service.dto";
import * as moment from 'moment';

export class CreateAppointmentDto {

    @IsNotEmpty()
    @Transform(({ value }) => new Date(value))
    @IsDate()
    @MinDate(new Date())
    bookedAt: number;

    @IsNotEmpty()
    @IsNumber()
    barberId: number;

    @ValidateNested({ each: true })
    @Type(() => CreateServiceDto)
    services: [CreateServiceDto];
}