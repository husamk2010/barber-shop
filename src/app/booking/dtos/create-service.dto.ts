import { IsNotEmpty, IsNumber } from "class-validator";

export class CreateServiceDto {

    @IsNotEmpty()
    @IsNumber()
    id: number;
}