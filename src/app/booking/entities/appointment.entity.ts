import { BarberService } from "src/app/barber-shop/entities/barber-service.entity";
import { BarberShop } from "src/app/barber-shop/entities/barber.entity";
import { Client } from "src/app/client/entities/client.entity";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

export enum AppointmentStatus {
    NOT_SHOW = 'NOT_SHOW',
    PENDING = 'PENDING',
    ACTIVE = 'ACTIVE',
    CANCELLED = 'CANCELLED',
    REJECTED = 'REJECTED',
    ACCEPTED = 'ACCEPTED'
}

@Entity()
export class Appointment {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        type: 'datetime'
    })
    bookedAt: Date;

    @Column({
        type: 'enum',
        enum: AppointmentStatus,
        default: AppointmentStatus.PENDING
    })
    status: AppointmentStatus;

    @ManyToOne(() => Client, (client) => client.id)
    client: Client;

    @ManyToOne(() => BarberShop, (barberShop) => barberShop.id)
    barber: BarberShop;
    
    @ManyToMany(() => BarberService)
    @JoinTable()
    services: BarberService[]
}