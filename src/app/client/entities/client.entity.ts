import { User } from "src/app/user/entities/user.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Client {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({
        default: ''
    })
    name: string;

    @ManyToOne(() => User, (user) => user.id)
    user: User;
}
