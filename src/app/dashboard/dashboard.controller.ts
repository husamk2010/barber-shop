import { Body, Controller, Get, HttpStatus, Post, UseGuards } from '@nestjs/common';
import { Roles } from 'src/core/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/core/guards/jwt-auth.guard';
import { RolesGuard } from 'src/core/guards/roles.guard';
import { SalonServiceService } from '../barber-shop/salon-service/salon-service.service';
import { UserTypes } from '../user/types/user.type';
import { DashboardService } from './dashboard.service';
import { AcceptOrRejectDto } from './dtos/accept-or-reject-documents.dto';

@Controller('dashboard')
export class DashboardController {
    constructor(
        private readonly dashboardService: DashboardService,
        private readonly salonService: SalonServiceService,
    ) { }

    // accept or reject barber documents
    @Post('/barber/docs/manage')
    @Roles(UserTypes.ADMIN)
    @UseGuards(JwtAuthGuard, RolesGuard)
    async manageDocuments(@Body() acceptOrRejectDto: AcceptOrRejectDto) {
        const result = await this.dashboardService.manageDocuments(acceptOrRejectDto);

        return {
            status: HttpStatus.OK,
            message: result
        }
    }

    @Get('/barber/rejections')
    @Roles(UserTypes.ADMIN)
    @UseGuards(JwtAuthGuard, RolesGuard)
    async getRejections() {
        const rejections = await this.dashboardService.getRejections();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: rejections
        }
    }



    @Get('barber/accounts')
    @Roles(UserTypes.ADMIN)
    @UseGuards(JwtAuthGuard, RolesGuard)
    async getAccounts() {
        const accounts = await this.dashboardService.getAccounts();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: accounts
        }
    }


    @Get('barber/services')
    async getServices() {
        const services = await this.salonService.getServices();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: services
        }
    }

}
