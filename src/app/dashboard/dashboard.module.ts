import { Module } from '@nestjs/common';
import { BarberShopModule } from '../barber-shop/barber-shop.module';
import { RejectionModule } from '../barber-shop/rejection/rejection.module';
import { UserModule } from '../user/user.module';
import { DashboardController } from './dashboard.controller';
import { DashboardService } from './dashboard.service';

@Module({
  imports: [
    BarberShopModule,
    UserModule,
    RejectionModule
  ],
  providers: [DashboardService],
  controllers: [DashboardController]
})
export class DashboardModule {}
