import { Injectable, NotFoundException } from '@nestjs/common';
import { BarberShopService } from '../barber-shop/barber-shop.service';
import { RejectionService } from '../barber-shop/rejection/rejection.service';
import { ProfileStatus } from '../barber-shop/types/salon.type';
import { AcceptOrRejectDto } from './dtos/accept-or-reject-documents.dto';
import { ChangeStatus } from './types/change-status.type';

@Injectable()
export class DashboardService {

    constructor(
        private readonly barbersService: BarberShopService,
        private readonly rejectionService: RejectionService,
    ) { }


    getAccounts() {
        return this.barbersService.find();
    }

    async manageDocuments({ barberId, status, reason }: AcceptOrRejectDto) {
        const barber = await this.barbersService.findOne(barberId);
        if (!barber) {
            throw new NotFoundException('Barber not found');
        }

        switch (status) {
            case ChangeStatus.APPROVE:
                await this.barbersService.update(barberId, {
                    profileStatus: ProfileStatus.APPROVED
                });
                return 'Documents has accepted';

            case ChangeStatus.REJECT:
                await this.barbersService.update(barberId, {
                    profileStatus: ProfileStatus.REJECTED
                });

                await this.rejectionService.create(reason, barber);
                return 'Documents has rejected';
        }
    }

    getRejections() {
        return this.rejectionService.find();
    }
}
