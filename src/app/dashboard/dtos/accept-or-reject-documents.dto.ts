import { IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString } from "class-validator";
import { ChangeStatus } from "../types/change-status.type";



export class AcceptOrRejectDto {

    @IsNumber()
    @IsNotEmpty()
    barberId: number;


    @IsEnum(ChangeStatus)
    @IsNotEmpty()
    status: ChangeStatus;


    @IsString()
    @IsNotEmpty()
    @IsOptional()
    reason: string;

}