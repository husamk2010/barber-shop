import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UtilityService } from '../util/util.service';
import { Image } from './entities/image.entity';

@Injectable()
export class ImageService {
    constructor(
        @InjectRepository(Image) private readonly imageRepository: Repository<Image>,
        private readonly utilService: UtilityService
    ) { }


    create(image: Image) {
        return this.imageRepository.save(image);
    }

    createList(images: Image[]){
        return this.imageRepository.save(images);
    }


    async delete(id: number) {
        const image = await this.imageRepository.findOneBy({ id });
        if (!image) {
            return;
        }
        await this.imageRepository.delete(id);
        this.utilService.removeFiles([image])
    }
}
