import { IsArray, IsNotEmpty, IsNumberString, IsOptional, IsString, IsUrl } from "class-validator";

export class CreatePromotionDto {

    @IsNotEmpty()
    @IsString()
    title: string;

    @IsOptional()
    @IsString()
    description?: string;

    @IsUrl()
    image: string;

    @IsArray()
    @IsNotEmpty()
    users: string[];
}