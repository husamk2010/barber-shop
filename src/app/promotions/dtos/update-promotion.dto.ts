import { IsArray, IsNumberString, IsOptional, IsString, IsUrl } from "class-validator";

export class UpdatePromotionDto {

    @IsOptional()
    @IsString()
    title: string;

    @IsOptional()
    @IsString()
    description: string;

    @IsOptional()
    @IsUrl()
    image: string;

    @IsOptional()
    @IsArray()
    users: string[];
}