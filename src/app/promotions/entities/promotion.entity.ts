import { BarberShop } from "src/app/barber-shop/entities/barber.entity";
import { Client } from "src/app/client/entities/client.entity";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

export enum PromotionStatus {
    PENDING = 'PENDING',
    ACTIVE = 'ACTIVE',
    SENT = 'SENT',
    CANCELLED = 'CANCELLED'
}

@Entity()
export class Promotion {

    @PrimaryGeneratedColumn()
    id: string;

    @Column()
    title: string;

    @Column({
        type: 'enum',
        enum: PromotionStatus,
        default: PromotionStatus.PENDING
    })
    status?: PromotionStatus;

    @Column({
        default: ''
    })
    description?: string;

    @Column({
        default: ''
    })
    image: string;


    @ManyToOne(() => BarberShop, (barberShop) => barberShop.id)
    owner: BarberShop;

    // TODO: handle promotions target
    // @ManyToMany(() => Client)
    // @JoinTable({ name: 'prmotion_client'})
    // target: Client[];
}