import { Body, Controller, Delete, Get, Head, Headers, Param, Post, Put } from '@nestjs/common';
import { BarberShop } from '../barber-shop/entities/barber.entity';
import { CreatePromotionDto } from './dtos/create-promotion.dto';
import { UpdatePromotionDto } from './dtos/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { PromotionsService } from './promotions.service';

@Controller('promotions')
export class PromotionsController {
  constructor(private readonly promotionsService: PromotionsService) { }

  @Get(':shopId')
  getPromotionsByShopId(@Param() shopId: string, @Headers('authorization') headers) {
    // return this.promotionsService.findByShopId(shopId);
  }

  @Post('create')
  createPromotionDto(@Headers('authorization') headers, @Body() createPromotionDto: CreatePromotionDto) {
    // return this.promotionsService.createPromotion(promotion);
  }

  @Delete(':id')
  deletePromotion(@Param() promotionId: string) {
    // return this.promotionsService.deletePromotion(promotionId);
  }


  @Put(':id')
  updatePromotion(@Param() promotionId, @Body() updatePromotionDto: UpdatePromotionDto) {
    // return this.promotionsService.updatePromotion(promotionId, promotion)
  }


  @Put(':id/send')
  sendPromotion(@Param() promotionId: string){
    // return this.promotionsService.sendPromotion(promotionId);
  }


  @Put(':id/cancel')
  cancelPromotion(@Param() promotionId: string){
    // return this.promotionsService.cancelPromotion(promotionId);
  }

}
