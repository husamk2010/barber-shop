import { Injectable } from '@nestjs/common';
import { Promotion, PromotionStatus } from './entities/promotion.entity';

@Injectable()
export class PromotionsService {
    // constructor(private readonly utilityService: UtilityServiceProvider){}

    private readonly promotions: Promotion[] = [];

    findByShopId(shopId: string){
        // return this.promotions.filter(promotion => promotion.owner.id === shopId);
    }

    createPromotion(promotion: Promotion){
        // promotion.id = this.utilityService.uniqueNumber();
        promotion.status = PromotionStatus.PENDING;
        this.promotions.push(promotion);
        return promotion;
    }


    deletePromotion(promotionId: string){
        this.promotions.splice(this.promotions.findIndex(promotion => promotion.id === promotionId), 1);
        return "Deleted successfully"
    }

    updatePromotion(promotionId: string, promotion: Promotion){
        const promotionIndex = this.promotions.findIndex(promotion => promotion.id === promotionId);
        if(promotionIndex > -1){
            this.promotions[promotionIndex] = {
                ...this.promotions[promotionIndex],
                ...promotion
            }
            return "Updated successfully"
        }
    }


    sendPromotion(promotionId: string){
        const promotionIndex = this.promotions.findIndex(promotion => promotion.id === promotionId);
        if(promotionIndex > -1){
            this.promotions[promotionIndex].status = PromotionStatus.ACTIVE;
        }

        return "Promotion has sent";
    }

    cancelPromotion(promotionId: string){
        const promotionIndex = this.promotions.findIndex(promotion => promotion.id === promotionId);
        if(promotionIndex > -1){
            this.promotions[promotionIndex].status = PromotionStatus.CANCELLED;
        }

        return "Promotion has cancelled";
    }


}
