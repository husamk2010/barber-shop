import { Injectable } from '@nestjs/common';
import { Region } from './entities/region.entity';

@Injectable()
export class RegionsService {
    private readonly regions: Region[] = [];

    importRegions(regions: Region[]){
        this.regions.push(...regions);
        return "Imported successfully";
    }

    getRegions(){
        return this.regions;
    }
}
