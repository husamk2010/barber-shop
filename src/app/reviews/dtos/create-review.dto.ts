import { IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString } from "class-validator";

export class CreateReviewDto{

    @IsOptional()
    @IsString()
    text?: string;


    @IsNotEmpty()
    @IsNumberString()
    shopId: string;


    @IsNotEmpty()
    @IsNumber()
    rate: number;
}