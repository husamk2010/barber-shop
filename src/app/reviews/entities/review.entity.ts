import { BarberShop } from "src/app/barber-shop/entities/barber.entity";
import { Client } from "src/app/client/entities/client.entity";

import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Review {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    rate: number;

    @Column()
    text: string;

    @ManyToOne(() => Client, (client) => client.id)
    client: Client;

    @ManyToOne(() => BarberShop, (barberShop) => barberShop.id)
    barber: BarberShop;
}