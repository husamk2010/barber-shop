import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { UserTypes } from "../types/user.type";


@Entity()
export class UserType {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column({
        type: 'enum',
        enum: UserTypes
    })
    code: UserTypes;
}
