import { Region } from "src/app/regions/entities/region.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserType } from "./user-type.entity";


@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ unique: true })
    phoneNumber: string;

    @ManyToOne(() => Region, (region) => region.id)
    region: Region;

    @ManyToOne(() => UserType, (userType) => userType.id)
    userType: UserType;

    @Column({
        default: false
    })
    signedUpWithGoogle: boolean;
}
