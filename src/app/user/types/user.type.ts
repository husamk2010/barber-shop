export enum UserTypes {
    CLIENT = 'client',
    SHOP = 'shop',
    ADMIN = 'admin'
}