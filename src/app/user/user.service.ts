import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UserType } from './entities/user-type.entity';
import { User } from './entities/user.entity';
import { UserTypes } from './types/user.type';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(UserType) private readonly userTypeRepository: Repository<UserType>,
    ) { }

    find() {
        return this.userRepository.find();
    }

    async create(user: User, type?: UserTypes) {
        const isExists = await this.findOneBy({ phoneNumber: user.phoneNumber });

        if (isExists) {
            throw new BadRequestException('user already exists');
        }

        let newUser = user;
        if (type) {
            newUser.userType = await this.userTypeRepository.findOneBy({ code: type });
        }
        return this.userRepository.save(newUser);
    }

    findOneBy(attr: any) {
        return this.userRepository.findOne({
            relations: {
                userType: true
            },
            where: attr,
            order: {
                id: 'DESC'
            }
        })
    }

    update(id: number, user: Partial<User>) {
        return this.userRepository.update(id, user);
    }
}
