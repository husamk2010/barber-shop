import { Global, Module } from '@nestjs/common';
import { UtilityService } from './util.service';


@Global()
@Module({
    providers: [
        UtilityService
    ],
    exports: [UtilityService]
})
export class UtilModule {}
