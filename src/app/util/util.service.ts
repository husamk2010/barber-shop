import { BadRequestException, Injectable } from "@nestjs/common";
import { unlink, writeFile } from "fs";
import { resolve } from "path";

@Injectable()
export class UtilityService {
    uniqueNumber(){
        return (Math.random() * 99999).toFixed() + Date.now()
    }

    isEmptyObject(obj: Object){
        for(let key in obj){
            if(obj[key] && obj.hasOwnProperty(key)){
                return false;
            }
        }

        return true;
    }

    uniqueFilename (filename) {
        return Date.now() + '-' + Math.round(Math.random() * 1E9) + '-' + filename.replace(/ /g, '_')
    }
    
    
    removeFiles(files: any[]) {
        files.forEach(file => {
            unlink(file.path, () => { })
        });
    
        console.log('Deleted files ', files.length);
    }
    
    
    saveFiles (files: any[]) {
        const basePath = resolve('.') + process.env.UPLOAD_DIRECTORY + '/';
        const savedFiles = [];
    
        return Promise
            .all(files.map(file => {
                let filename = this.uniqueFilename(file.originalname);
                let path = basePath + filename;
    
                return new Promise((res, rej) => {
                    writeFile(path, file.buffer, (err) => {
                        if (err)
                            rej('Failed to save the file');
    
                        savedFiles.push({ name: file.originalname, path });
                        return res({ path, url: process.env.HOSTNAME + '/' + filename, name: file.originalname })
                    })
                })
            }))
            .catch(err => {
                this.removeFiles(savedFiles);
                throw new BadRequestException('Something went wrong')
            })
    }
}