import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const BarberDecorator = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const barber = request.barber;

    return data ? barber[data] : barber;
  },
);