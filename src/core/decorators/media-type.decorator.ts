import { SetMetadata } from '@nestjs/common';

export enum Media_Size {
    _1MEGABYTE = 1000 * 1000 * 1,
    _10MEGABYTE = 1000 * 1000 * 10,
    _50MEGABYTE = 1000 * 1000 * 50
}


const Media_Regex = {
    'media': /(image|video)\/(jpg|jpeg|bmp|gif|png|mp4)$/i,
    'files': /(application)\/(pdf|msword|vnd.openxmlformats-officedocument.wordprocessingml.document)$/i,
}

export enum Media_Type {
    MEDIA = 'media',
    FILES = 'files',
}

export const MEDIA_TYPE = 'media_type';
export const MediaType = (type: Media_Type) => SetMetadata(MEDIA_TYPE, Media_Regex[type]);


export const MEDIA_MAX_SIZE = 'media_max_size';
export const MediaSize = (size: Media_Size) => SetMetadata(MEDIA_MAX_SIZE, size);

