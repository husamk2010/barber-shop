import { SetMetadata } from '@nestjs/common';
import { ProfileStatus } from 'src/app/barber-shop/types/salon.type';


export const PROFILE_KEY = 'profile';
export const Profile = (...profileStatus: ProfileStatus[]) => SetMetadata(PROFILE_KEY, profileStatus);