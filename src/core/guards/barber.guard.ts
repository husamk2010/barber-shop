import { Injectable, CanActivate, ExecutionContext, Inject, NotFoundException } from '@nestjs/common';
import { BarberShopService } from 'src/app/barber-shop/barber-shop.service';

@Injectable()
export class BarberGuard implements CanActivate {
  constructor(
    @Inject(BarberShopService) private readonly barberService: BarberShopService
  ) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const user = request.user;
    
    const barber = await this.barberService.findByUser(user.id);
    
    if(!barber){
      throw new NotFoundException('barber not found')
    }

    request.barber = barber;

    return true;
  }
}