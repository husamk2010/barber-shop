import { Injectable, CanActivate, ExecutionContext, Inject, NotFoundException } from '@nestjs/common';
import { ClientService } from 'src/app/client/client.service';

@Injectable()
export class ClientGuard implements CanActivate {
  constructor(
    @Inject(ClientService) private readonly clientService: ClientService
  ) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();

    const user = request.user;
    
    const client = await this.clientService.findByUser(user.id);
    if(!client){
      throw new NotFoundException('client not found')
    }

    request.client = client;

    return true;
  }
}