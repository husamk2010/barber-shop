import { Injectable, CanActivate, ExecutionContext, Inject, NotFoundException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { BarberShopService } from 'src/app/barber-shop/barber-shop.service';
import { ProfileStatus } from 'src/app/barber-shop/types/salon.type';
import { PROFILE_KEY } from '../decorators/profile.decorator';

@Injectable()
export class ProfileGuard implements CanActivate {
  constructor(
    @Inject(BarberShopService) private readonly barberService: BarberShopService,
    private reflector: Reflector,
  ) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const profileStatues = this.reflector.getAllAndOverride<ProfileStatus[]>(PROFILE_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (!profileStatues) {
      return true;
    }
    const request = context.switchToHttp().getRequest();

    const user = request.user;
    
    const barber = await this.barberService.findByUser(user.id);

    if(!barber){
      throw new NotFoundException('barber not found')
    }

    const result = profileStatues.some((status) => barber.profileStatus === status);
    
    if(result){
      request.barber = barber;
      return true;
    }

    return false;

  }
}