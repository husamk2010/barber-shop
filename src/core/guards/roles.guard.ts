import { Injectable, CanActivate, ExecutionContext, Inject, UnauthorizedException } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { UserTypes } from 'src/app/user/types/user.type';
import { UserService } from 'src/app/user/user.service';
import { ROLES_KEY } from '../decorators/roles.decorator';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    @Inject(UserService) private readonly usersService: UserService,
    private reflector: Reflector
  ) { }

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const requiredRoles = this.reflector.getAllAndOverride<UserTypes[]>(ROLES_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!requiredRoles) {
      return true;
    }
    const { user } = context.switchToHttp().getRequest();

    const userObject = await this.usersService.findOneBy({ phoneNumber: user.phoneNumber });

    if (!userObject) {
      throw new UnauthorizedException('invalid user');
    }
    
    
    const result = requiredRoles.some((role) => user?.role === role);
    return result;

  }
}