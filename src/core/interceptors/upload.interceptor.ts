import { BadRequestException, CallHandler, ExecutionContext, Inject, NestInterceptor } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { Observable } from "rxjs";
import { UtilityService } from "src/app/util/util.service";
import { MEDIA_MAX_SIZE, MEDIA_TYPE } from "../decorators/media-type.decorator";

export class UploadInterceptor implements NestInterceptor {
    constructor(
        @Inject(UtilityService) private readonly utilService: UtilityService,
        private reflector: Reflector,
    ) { }

    async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<any>> {

        const request = context.switchToHttp().getRequest();
        const files: any[] = request.files ? request.files : request.file ? [request.file] : [];

        if (!files || !files.length) {
            throw new BadRequestException('Invalid files')
        }

        const mediaRegex = this.reflector.getAllAndOverride<RegExp>(MEDIA_TYPE, [
            context.getHandler(),
            context.getClass(),
        ]);

        const mediaSize = this.reflector.getAllAndOverride<string>(MEDIA_MAX_SIZE, [
            context.getHandler(),
            context.getClass(),
        ]);


        if (mediaRegex || mediaSize) {
            files.forEach(file => {

                if (mediaSize && file.size > mediaSize) {
                    throw new BadRequestException('file is too big')
                }
                if (mediaRegex && !mediaRegex.test(file.mimetype)) {
                    throw new BadRequestException('unsupported media type')
                }
            })
        }


        request.uploadedFiles = await this.utilService.saveFiles(files);


        return next.handle();
    }
}