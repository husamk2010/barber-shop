import { BadRequestException, CallHandler, ExecutionContext, Injectable, NestInterceptor, Scope, UnauthorizedException } from "@nestjs/common";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';
import { resolve } from 'path';
import { UserService } from "src/app/user/user.service";



@Injectable({ scope: Scope.REQUEST })
export class UserInterceptor implements NestInterceptor {
    constructor(private readonly usersService: UserService){}

    async intercept(context: ExecutionContext, next: CallHandler<any>): Promise<Observable<any>> {
        const request = context.switchToHttp().getRequest();
        const payload = request.user;


        const user = await this.usersService.findOneBy({ phoneNumber: payload.phoneNumber});

        if(!user){
            throw new UnauthorizedException('invalid user');
        }
        
        request.user = user;
        
        
        return next.handle();
    }
}