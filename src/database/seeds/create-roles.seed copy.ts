import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { UserType } from 'src/app/user/entities/user-type.entity';
import { UserTypes } from 'src/app/user/types/user.type';

export default class CreateRolesSeeder implements Seeder {
    public async run(
        dataSource: DataSource,
        factoryManager: SeederFactoryManager
    ): Promise<any> {
        const repository =  dataSource.getRepository(UserType);
        await repository.insert([
            {
                name: 'client',
                code: UserTypes.CLIENT
            },
            {
                name: 'shop',
                code: UserTypes.SHOP
            },
            {
                name: 'admin',
                code: UserTypes.ADMIN
            },
        ]);
    }
}