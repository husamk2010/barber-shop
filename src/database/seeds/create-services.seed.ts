import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { Service } from 'src/app/barber-shop/entities/service.entity';

export default class CreateServicesSeeder implements Seeder {
    public async run(
        dataSource: DataSource,
        factoryManager: SeederFactoryManager
    ): Promise<any> {
        const repository =  dataSource.getRepository(Service);
        await repository.insert([
            {
                title: 'Hair cut'
            },
            {
                title: 'Beard cut'
            },
            {
                title: 'Skin care'
            },
            {
                title: 'Nails care'
            }
        ]);
    }
}