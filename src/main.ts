import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { readFileSync } from 'fs';
import * as express from 'express';
import { ExpressAdapter } from '@nestjs/platform-express';
import * as http from 'http'; 
import * as https from 'https'; 

async function bootstrap() {

  // TODO: check later
  let httpsOptions;
  if (process.env.NODE_NEV === 'production') {
    httpsOptions = {
      key: readFileSync(process.env.SSL_KEY, 'utf-8'),
      cert: readFileSync(process.env.SSL_CERT, 'utf-8'),
    };

    console.log(httpsOptions, 'options')
  }

  const server = express();


  const app = await NestFactory.create(
    AppModule,
    new ExpressAdapter(server),
  );

  app.init();
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();

  http.createServer(server).listen(process.env.APP_PORT);
  // https.createServer(httpsOptions, server).listen(443);
}
bootstrap();
